#!/bin/bash

###
# Install base packages and applications
###

sudo apt install \
-y \
calibre `#Ebook management` \
chrome-gnome-shell `#Gnome extention to install extensions in browsers`
darktable `#Easy RAW Editor` \
exfat-utils `#Allows managing exfat (android sd cards and co)` \
ffmpeg `#Adds Codec Support to Firefox, and in general` \
filezilla `#S/FTP Access` \
exfat-fuse `#Exfat drive enablement` \
exfat-utils `#Exfat drive utilities` \
gimp `#The Image Editing Powerhouse - and its plugins` \
git `#VCS done right` \
gnome-tweak-tool `#Your central place to make gnome like you want` \
gsconnect \
htop `#Cli process monitor` \
inkscape  `#Working with .svg files` \
kdenlive  `#Advanced Video Editor` \
keepassxc  `#Password Manager` \
krita  `#Painting done right keep in mind mypaint and gimp cant work together in current upstream versions - yet` \
mpv `#The best media player (with simple gui)` \
mumble `#Talk with your friends` \
openshot `#Simple Video Editor` \
p7zip `#Archives` \
python3 `#Pythons 3 install` \
rawtherapee `#Professional RAW Editor` \
telegram-desktop `#Chatting, with newer openssl and qt base!` \
tuned `#Tuned can optimize your performance according to metrics. tuned-adm profile powersave can help you on laptops, alot` \
virtualbox `#Virtualbox for virtual machines` \
virtualbox-ext-pack `#VirtualBox extension packs for additional functionality` \
virtualbox-guest-source `#Virtualbox guest sources` \
wavemon `#a cli wifi status tool` \
youtube-dl `#Allows you to download and save youtube videos but also to open their links by dragging them into mpv!`
qbittorrent `#QBittorrent install` \

###
# Snap installs
###

sudo snap install vlc
sudo snap install gimp 
sudo snap install spotify 
sudo snap install darktable
sudo snap install discord
sudo snap install qownnotes
sudo snap install signal-desktop

###
# These are more targeted to developers/advanced Users/specific usecases, you might want them - or not.
###
sudo apt install \
-y \
fortune-mod `#Inspiring Quotes` \
hexchat `#Irc Client` \
ncdu `#Directory listing CLI tool. For a gui version take a look at "baobab"` \
nextcloud-client `#Nextcloud Integration for Fedora` \
nextcloud-client-nautilus `#Also for the File Manager, shows you file status` \
syncthing-gtk `#Syncing evolved - to use the local only mode open up the ports with firewall-cmd --add-port=port/tcp --permanent && firewall-cmd --reload` 

###
# Remove some un-needed stuff
###

#sudo dnf remove \
#-y \


###
# Enable some of the goodies, but not all
# Its the users responsibility to choose and enable zsh, with oh-my-zsh for example
# or set a more specific tuned profile
###

#sudo systemctl enable --now tuned
#sudo tuned-adm profile balanced

#Performance:
#sudo tuned-adm profile desktop

#Virtual Machine Host:
#sudo tuned-adm profile virtual-host

#Virtual Machine Guest:
#sudo tuned-adm profile virtual-guest

#Battery Saving:
#sudo tuned-adm profile powersave